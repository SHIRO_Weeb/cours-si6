﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class frmAjoutChambre : Form
    {
        public frmAjoutChambre()
        {
            InitializeComponent();
            foreach(Hotel Item  in Persistance.getLesHotels())
            {
                ComboHotel.Items.Add(Item.Nom);  
            } 
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {   
            foreach(Hotel Item in Persistance.getLesHotels())
            {
                if (Item.Nom == ComboHotel.Text)
                {
                    int Id = Item.Id;
                    Persistance.ajouteChambre(Id, numEtage.Value, txtDesc.Text);
                }
            }
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            numEtage.Value = 0;
            txtDesc.Text = "";
            ComboHotel.Text = "";
        }

        private void btnFermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
