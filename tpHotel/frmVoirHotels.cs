﻿using System;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class frmVoirHotels : Form
    {
        public frmVoirHotels()
        {
            InitializeComponent();
            lstHotels.DataSource = Persistance.getLesHotels();
        }

        private void btnFermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
