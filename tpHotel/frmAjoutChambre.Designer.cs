﻿namespace tpHotel
{
    partial class frmAjoutChambre
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numEtage = new System.Windows.Forms.NumericUpDown();
            this.txtDesc = new System.Windows.Forms.TextBox();
            this.ComboHotel = new System.Windows.Forms.ComboBox();
            this.lnlEtage = new System.Windows.Forms.Label();
            this.lblDesc = new System.Windows.Forms.Label();
            this.lblHotel = new System.Windows.Forms.Label();
            this.btnEnregistrer = new System.Windows.Forms.Button();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.btnFermer = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numEtage)).BeginInit();
            this.SuspendLayout();
            // 
            // numEtage
            // 
            this.numEtage.Location = new System.Drawing.Point(99, 67);
            this.numEtage.Name = "numEtage";
            this.numEtage.Size = new System.Drawing.Size(120, 20);
            this.numEtage.TabIndex = 0;
            // 
            // txtDesc
            // 
            this.txtDesc.Location = new System.Drawing.Point(99, 116);
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.Size = new System.Drawing.Size(120, 20);
            this.txtDesc.TabIndex = 1;
            // 
            // ComboHotel
            // 
            this.ComboHotel.FormattingEnabled = true;
            this.ComboHotel.Location = new System.Drawing.Point(99, 161);
            this.ComboHotel.Name = "ComboHotel";
            this.ComboHotel.Size = new System.Drawing.Size(121, 21);
            this.ComboHotel.TabIndex = 2;
            // 
            // lnlEtage
            // 
            this.lnlEtage.AutoSize = true;
            this.lnlEtage.Location = new System.Drawing.Point(26, 69);
            this.lnlEtage.Name = "lnlEtage";
            this.lnlEtage.Size = new System.Drawing.Size(35, 13);
            this.lnlEtage.TabIndex = 3;
            this.lnlEtage.Text = "Étage";
            // 
            // lblDesc
            // 
            this.lblDesc.AutoSize = true;
            this.lblDesc.Location = new System.Drawing.Point(22, 119);
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Size = new System.Drawing.Size(60, 13);
            this.lblDesc.TabIndex = 4;
            this.lblDesc.Text = "Déscription";
            // 
            // lblHotel
            // 
            this.lblHotel.AutoSize = true;
            this.lblHotel.Location = new System.Drawing.Point(26, 164);
            this.lblHotel.Name = "lblHotel";
            this.lblHotel.Size = new System.Drawing.Size(32, 13);
            this.lblHotel.TabIndex = 5;
            this.lblHotel.Text = "Hôtel";
            // 
            // btnEnregistrer
            // 
            this.btnEnregistrer.Location = new System.Drawing.Point(25, 253);
            this.btnEnregistrer.Name = "btnEnregistrer";
            this.btnEnregistrer.Size = new System.Drawing.Size(75, 23);
            this.btnEnregistrer.TabIndex = 6;
            this.btnEnregistrer.Text = "Enregistrer";
            this.btnEnregistrer.UseVisualStyleBackColor = true;
            this.btnEnregistrer.Click += new System.EventHandler(this.btnEnregistrer_Click);
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Location = new System.Drawing.Point(121, 253);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(75, 23);
            this.btnAnnuler.TabIndex = 7;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = true;
            this.btnAnnuler.Click += new System.EventHandler(this.btnAnnuler_Click);
            // 
            // btnFermer
            // 
            this.btnFermer.Location = new System.Drawing.Point(221, 253);
            this.btnFermer.Name = "btnFermer";
            this.btnFermer.Size = new System.Drawing.Size(75, 23);
            this.btnFermer.TabIndex = 8;
            this.btnFermer.Text = "Fermer";
            this.btnFermer.UseVisualStyleBackColor = true;
            this.btnFermer.Click += new System.EventHandler(this.btnFermer_Click);
            // 
            // frmAjoutChambre
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(327, 348);
            this.Controls.Add(this.btnFermer);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.btnEnregistrer);
            this.Controls.Add(this.lblHotel);
            this.Controls.Add(this.lblDesc);
            this.Controls.Add(this.lnlEtage);
            this.Controls.Add(this.ComboHotel);
            this.Controls.Add(this.txtDesc);
            this.Controls.Add(this.numEtage);
            this.Name = "frmAjoutChambre";
            this.Text = "frmAjoutChambre";
            ((System.ComponentModel.ISupportInitialize)(this.numEtage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numEtage;
        private System.Windows.Forms.TextBox txtDesc;
        private System.Windows.Forms.ComboBox ComboHotel;
        private System.Windows.Forms.Label lnlEtage;
        private System.Windows.Forms.Label lblDesc;
        private System.Windows.Forms.Label lblHotel;
        private System.Windows.Forms.Button btnEnregistrer;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.Button btnFermer;
    }
}