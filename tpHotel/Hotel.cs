﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpHotel
{
     public class Hotel
    {
        private int id;
        private string nom;
        private string adresse;
        private string ville;

        

        public Hotel(int id, string nom, string adresse, string ville)
        {
            this.id = id;
            this.nom = nom;
            this.adresse = adresse;
            this.ville = ville;
        }

        public int Id { get => id; set => id = value; }
        public string Nom { get => nom; set => nom = value; }
        public string Adresse { get => adresse; set => adresse = value; }
        public string Ville { get => ville; set => ville = value; }
    }
    public class Chambre
    {
        int num;
        int etage;
        string description;
        Hotel hotel;
        

        public Chambre(int num ,int id,int etage, string description)
        {
            this.Etage = etage;
            this.Description = description;
            this.Num = num;
            foreach(Hotel Item in Persistance.getLesHotels())
            {
                if (id == Item.Id)
                {
                    hotel = Item;
                }       
            }

        }
        public int Num { get => num; set => num = value; }
        public int Etage { get => etage; set => etage = value; }
        public string Description { get => description; set => description = value; }
        public string NomHotel { get => hotel.Nom; }
    }
}
