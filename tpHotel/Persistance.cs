﻿using System;
using System.Data.SqlClient;
using System.Collections;

namespace tpHotel
{
    static class Persistance
    {
        private static SqlConnection connexionSql()
        {
            string server = "192.168.1.28";
            string database = "hotelBuffet";
            string uid = "lmct65";
            string password = "lmct65";

            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" + database + ";";
            connectionString += "UID=" + uid + ";" + "PASSWORD=" + password + ";";

            SqlConnection conn = new SqlConnection(connectionString);

            return conn;
        }


        public static void ajouteHotel(String nom, String adresse, String ville)
        {
            SqlConnection conn = Persistance.connexionSql();
            conn.Open();
            string requete = "insert into hotel (nom,adresse,ville) ";
            requete += "Values('" + nom + "','" + adresse + "','" + ville + "')";
            System.Windows.Forms.MessageBox.Show(requete);
            SqlCommand cmd = new SqlCommand(requete, conn);
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public static void ajouteChambre(int idHotel,decimal Etage, String Description)
        {
            SqlConnection conn = Persistance.connexionSql();
            conn.Open();
            string requete = "insert into chambre (idHotel,etage,description) ";
            requete += "Values('" + idHotel + "','" + Etage + "','"+ Description + "')";

            System.Windows.Forms.MessageBox.Show(requete);
            SqlCommand cmd = new SqlCommand(requete, conn);
            
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public static ArrayList getLesHotels()
        {
            ArrayList liste = new ArrayList();
            SqlConnection conn = Persistance.connexionSql();
            conn.Open();
            string requete = ("select * from hotel");
            SqlCommand cmd = new SqlCommand(requete, conn);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                liste.Add(new Hotel(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3)));
            }
            conn.Close();
            return liste;
        }
        public static ArrayList getLesChambres()
        {
            ArrayList liste = new ArrayList();
            SqlConnection conn = Persistance.connexionSql();
            conn.Open();
            string requete = ("select * from chambre");
            SqlCommand cmd = new SqlCommand(requete, conn);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                liste.Add(new Chambre(reader.GetInt32(0), reader.GetInt32(1), reader.GetInt32(2), reader.GetString(3)));
            }
            conn.Close();
            return liste;
        }
    }
}